import { genericFunctionArrow } from '../generics/generics';
import { Villain, Hero } from '../interfaces';
// import { Hero } from './interfaces/Hero';
// import { Villain } from './interfaces/Villain';


const deadpool = {
    name: 'Deadpool',
    realName: 'Wade Winston Wilson',
    dangerLevel: 60
}

console.log(genericFunctionArrow<Hero>(deadpool).realName);
console.log(genericFunctionArrow<Villain>(deadpool).dangerLevel);